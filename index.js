const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const { MongoClient, ObjectId } = require('mongodb');
const request = require('request');
const fetch = require('node-fetch');
const fs = require('fs');
const Path = require('path');
const axios = require('axios');
const TelegramPassport = require('telegram-passport')
const pem = fs.readFileSync('./private.key').toString();
const passport = new TelegramPassport(pem);
const TG = require('telegram-bot-api');
const { Telegraf } = require('telegraf')
const botToken = "1801110407:AAHDQCsfgdHcxrlAFZ8_SYUoHPsJo3Jagqw"; //AfricaFinex KYC bot
const URL = 'https://african-finex-server.herokuapp.com';
const api = new TG({
    token: botToken
})
const bot = new Telegraf(botToken)
bot.telegram.setWebhook(`${URL}/${botToken}`);
// bot.startWebhook(`/${botToken}`, null, process.env.PORT || 4585)


// chat id
const myId = "974639281";
const chefId = "473816971";
const investor1 = "46672971"
const investor2 = "169690653";
const investor3 = "1058961870";
const AOAPartner = "551345224";
// const STNPartner = "143717891";



const uri = `mongodb+srv://africaFinexAdmin:50114400finex@cluster0.ukskk.mongodb.net/african-finex?retryWrites=true&w=majority`;
const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });

const app = express();
app.use(bot.webhookCallback(`/${botToken}`))
app.use(cors());
app.use(express.static('services'))
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));


// bot on start and reply
bot.start((ctx) => {
    const msg = `Welcome to Africafinex KYC's verification bot
Please go back to africaswap.org and press <b>Login with Telegram Passport</b>`

    api.sendPhoto({
        chat_id: ctx.update.message.from.id,
        caption: msg,
        parse_mode: 'html',
        photo: fs.createReadStream('./loginPaaport.jpg')
    })
        .then(res => console.log("success"))
        .catch(err => console.log(err))

})

bot.on("text", ctx => {
    if (ctx?.update?.message?.text?.split(" ")[0] === '/Id:') {
        const userId = ctx.update.message.text.split(" ")[1];
        const customMsg = ctx.update.message.text.split(" ").slice(2,).join(" ");

        api.sendMessage({
            chat_id: userId,
            text: customMsg,
            parse_mode: 'Markdown',
            reply_markup: {
                inline_keyboard: [
                    [
                        {
                            text: 'Contact us',
                            url: `https://t.me/joinchat/zzkF9XmFwFQ0NmQx`
                        }
                    ]
                ]
            }
        })
            .then(res => console.log("success"))
            .catch(err => console.log(err))
    }
})


bot.on('callback_query', (ctx) => {
    if (ctx?.update?.callback_query?.data.startsWith("/failedKyc ")) {
        const fromId = ctx.update.callback_query.from.id;
        const customerId = ctx?.update?.callback_query?.data.split(" ")[1];
        const msg = "What is the reason of failed KYC?"

        api.sendMessage({
            chat_id: fromId,
            text: msg,
            parse_mode: 'Markdown',
            reply_markup: {
                inline_keyboard: [
                    [
                        {
                            text: 'Some images are blur?',
                            callback_data: `/replyFailed notClear ${customerId}`
                        },
                    ],
                    [
                        {
                            text: 'Documents missing?',
                            callback_data: `/replyFailed notSent ${customerId}`
                        },
                    ],
                    [
                        {
                            text: 'Send custom message',
                            callback_data: `/replyFailed noKyc ${customerId}`
                        }
                    ]
                ]
            }
        })
            .then(res => console.log("success"))
            .catch(err => console.log(err))

    }

    else if (ctx?.update?.callback_query?.data.startsWith("/successKyc ")) {
        const userId = ctx?.update?.callback_query?.data.split(" ")[1]
        const successMsg = `Your KYC process was completed successfully.`

        api.sendMessage({
            chat_id: userId,
            text: successMsg,
            parse_mode: 'Markdown',
        })
            .then(res => console.log("success"))
            .catch(err => console.log(err))

    }


    else if (ctx?.update?.callback_query?.data.startsWith("/replyFailed ")) {
        const fromId = ctx.update.callback_query.from.id;
        const replyAra = ctx?.update?.callback_query?.data.split(" ");
        const chatId = replyAra[2];
        const reason = replyAra[1];
        const failedReason = reason === "notClear" ? "Images are not clear. We can't verify your KYC with these images." : reason === "notSent" ? "Some documents are missing. We haven't received all the necessary documents to verify your KYC." : "";
        const errorMsg = `KYC verification failed!!

${failedReason}

Unfortunately, we can't approve your transaction. Please try again and be careful when uploading images. A blurred image can result in an unsuccessful KYC.
    
If you have already transferred the amount to the given wallet/IBAN then click the "Contact us" button to get help form our group.`

        if (reason !== 'noKyc') {
            api.sendMessage({
                chat_id: chatId,
                text: errorMsg,
                parse_mode: 'Markdown',
                reply_markup: {
                    inline_keyboard: [
                        [
                            {
                                text: 'Contact us',
                                url: `https://t.me/joinchat/zzkF9XmFwFQ0NmQx`
                            }
                        ]
                    ]
                }
            })
                .then(res => console.log("success"))
                .catch(err => console.log(err))
        }

        else if (reason === 'noKyc') {
            const adminMsg = `Send your message to the customer about failed KYC.

Please put this command with your message..

/Id: ${chatId} "your message here.."
`

            api.sendMessage({
                chat_id: fromId,
                text: adminMsg,
                parse_mode: 'html',
            })
                .then(res => console.log("success"))
                .catch(err => console.log(err))
        }

    }
})

bot.catch(err => {
    console.log(err)
})


// Buy and Sell msg for AfricaFinex bot
const transactionMsg = (message, transactionObj) => {

    request(`https://api.telegram.org/bot1646880257:AAGajpBS7HzzUn3k9EdMpcGlIw3cNKpPqB4/sendMessage?chat_id=974639281&text=${message}`, function (error, response, body) {
        console.log('Success');
    });
    request(`https://api.telegram.org/bot1646880257:AAGajpBS7HzzUn3k9EdMpcGlIw3cNKpPqB4/sendMessage?chat_id=${chefId}&text=${message}`, function (error, response, body) {
        console.log('Success');
    });
    request(`https://api.telegram.org/bot1646880257:AAGajpBS7HzzUn3k9EdMpcGlIw3cNKpPqB4/sendMessage?chat_id=${investor1}8&text=${message}`, function (error, response, body) {
        console.log('Success');
    });

    request(`https://api.telegram.org/bot1646880257:AAGajpBS7HzzUn3k9EdMpcGlIw3cNKpPqB4/sendMessage?chat_id=${investor2}8&text=${message}`, function (error, response, body) {
        console.log('Success');
    });

    request(`https://api.telegram.org/bot1646880257:AAGajpBS7HzzUn3k9EdMpcGlIw3cNKpPqB4/sendMessage?chat_id=${investor3}&text=${message}`, function (error, response, body) {
        console.log('Success');
    });



    if (transactionObj?.currency === 'AOA') {
        request(`https://api.telegram.org/bot1646880257:AAGajpBS7HzzUn3k9EdMpcGlIw3cNKpPqB4/sendMessage?chat_id=${AOAPartner}&text=${message}`, function (error, response, body) {
            console.log('Success');
        });
    }
    // else if (transactionObj?.currency === 'STN') {
    //     request(`https://api.telegram.org/bot1646880257:AAGajpBS7HzzUn3k9EdMpcGlIw3cNKpPqB4/sendMessage?chat_id=${STNPartner}&text=${message}`, function (error, response, body) {
    //         console.log('Success');
    //     });
    // }
}



// download telegram file
const download = async (url, path) => {
    const response = await axios({
        method: 'GET',
        url: url,
        responseType: "stream"
    })

    await response.data.pipe(fs.createWriteStream(path));

    return new Promise((resolve, reject) => {
        response.data.on("end", () => resolve());
        response.data.on("error", err => reject(err))
    })
}


// kyc documents send
const KycSend = (decryptedPasswordData, phoneNum, country, teleName, teleId, teleUserName) => {

    return new Promise((resolve, reject) => {
        const credentialMsg = `Phone number: +${phoneNum}
Telegram name: ${teleName}
Telegram Id: ${teleId}
Telegram username: ${teleUserName}`

        // front image
        let frontId;
        let frontSecret;
        let frontHash;
        let frontPath;

        let backId;
        let backSecret;
        let backHash;
        let backPath;

        let selfieId;
        let selfieSecret;
        let selfieHash;
        let selfiePath;

        let resedintialId;
        let resedintialSecret;
        let resedintialHash;
        let resedintialPath;

        if (decryptedPasswordData?.hasOwnProperty('passport')) {
            frontId = decryptedPasswordData.passport.front_side.file.file_id;
            frontSecret = decryptedPasswordData.passport.front_side.secret;
            frontHash = decryptedPasswordData.passport.front_side.hash;
            frontPath = Path.resolve(__dirname, "image", "frontId.jpg")

            selfieId = decryptedPasswordData.passport.selfie.file.file_id;
            selfieSecret = decryptedPasswordData.passport.selfie.secret;
            selfieHash = decryptedPasswordData.passport.selfie.hash;
            selfiePath = Path.resolve(__dirname, "image", "selfie.jpg")

        }
        else if (decryptedPasswordData?.hasOwnProperty('driver_license')) {
            frontId = decryptedPasswordData.driver_license.front_side.file.file_id;
            frontSecret = decryptedPasswordData.driver_license.front_side.secret;
            frontHash = decryptedPasswordData.driver_license.front_side.hash;
            frontPath = Path.resolve(__dirname, "image", "frontId.jpg")

            backId = decryptedPasswordData.driver_license.reverse_side.file.file_id;
            backSecret = decryptedPasswordData.driver_license.reverse_side.secret;
            backHash = decryptedPasswordData.driver_license.reverse_side.hash;
            backPath = Path.resolve(__dirname, "image", "reverseId.jpg")


            selfieId = decryptedPasswordData.driver_license.selfie.file.file_id;
            selfieSecret = decryptedPasswordData.driver_license.selfie.secret;
            selfieHash = decryptedPasswordData.driver_license.selfie.hash;
            selfiePath = Path.resolve(__dirname, "image", "selfie.jpg")

        }
        else if (decryptedPasswordData?.hasOwnProperty('identity_card')) {
            frontId = decryptedPasswordData.identity_card.front_side.file.file_id;
            frontSecret = decryptedPasswordData.identity_card.front_side.secret;
            frontHash = decryptedPasswordData.identity_card.front_side.hash;
            frontPath = Path.resolve(__dirname, "image", "frontId.jpg")

            backId = decryptedPasswordData.identity_card.reverse_side.file.file_id;
            backSecret = decryptedPasswordData.identity_card.reverse_side.secret;
            backHash = decryptedPasswordData.identity_card.reverse_side.hash;
            backPath = Path.resolve(__dirname, "image", "reverseId.jpg")


            selfieId = decryptedPasswordData.identity_card.selfie.file.file_id;
            selfieSecret = decryptedPasswordData.identity_card.selfie.secret;
            selfieHash = decryptedPasswordData.identity_card.selfie.hash;
            selfiePath = Path.resolve(__dirname, "image", "selfie.jpg")

        }


        if (decryptedPasswordData?.utility_bill?.files.length) {
            resedintialId = decryptedPasswordData?.utility_bill?.files[0]?.file?.file_id;
            resedintialSecret = decryptedPasswordData.utility_bill.files[0].secret;
            resedintialHash = decryptedPasswordData.utility_bill.files[0].hash;
            resedintialPath = Path.resolve(__dirname, "image", "resedintal.jpg")
        }
        else if (decryptedPasswordData?.bank_statement?.files.length) {
            resedintialId = decryptedPasswordData?.bank_statement?.files[0]?.file?.file_id;
            resedintialSecret = decryptedPasswordData.bank_statement.files[0].secret;
            resedintialHash = decryptedPasswordData.bank_statement.files[0].hash;
            resedintialPath = Path.resolve(__dirname, "image", "resedintal.jpg")
        }
        else if (decryptedPasswordData?.rental_agreement?.files.length) {
            resedintialId = decryptedPasswordData?.rental_agreement?.files[0]?.file?.file_id;
            resedintialSecret = decryptedPasswordData.rental_agreement.files[0].secret;
            resedintialHash = decryptedPasswordData.rental_agreement.files[0].hash;
            resedintialPath = Path.resolve(__dirname, "image", "resedintal.jpg")
        }


        fetch(`https://api.telegram.org/bot${botToken}/getFile?file_id=${frontId}`)
            .then(res => res.json())
            .then(data => {

                const url = `https://api.telegram.org/file/bot${botToken}/${data.result.file_path}`;

                download(url, frontPath).then(() => {

                    const promise = fs.promises.readFile('./image/frontId.jpg');

                    Promise.resolve(promise).then(function (frontIdBuf) {


                        const frontIdDecode = passport.decryptPassportCredentials(
                            frontIdBuf, // Should be a Buffer
                            Buffer.from(frontHash, "base64"),
                            Buffer.from(frontSecret, "base64")
                        )

                        fs.writeFileSync('./decoded/frontId.jpg', frontIdDecode)

                        api.sendPhoto({
                            chat_id: myId,
                            caption: 'Front Side Document',
                            photo: fs.createReadStream('./decoded/frontId.jpg')
                        })
                            .then(res => console.log("success"))
                            .catch(err => console.log(err))

                        // chef's id
                        api.sendPhoto({
                            chat_id: chefId,
                            caption: 'Front Side Document',
                            photo: fs.createReadStream('./decoded/frontId.jpg')
                        })
                            .then(res => console.log("success"))
                            .catch(err => console.log(err))


                        if (country === 'AOA') {
                            api.sendPhoto({
                                chat_id: AOAPartner,
                                caption: 'Front Side Document',
                                photo: fs.createReadStream('./decoded/frontId.jpg')
                            })
                                .then(res => console.log("success"))
                                .catch(err => console.log(err))
                        }

                        // else if (country === 'STN') {
                        //     api.sendPhoto({
                        //         chat_id: STNPartner,
                        //            caption: 'Front Side Document',
                        //         photo: fs.createReadStream('./decoded/frontId.jpg')
                        //     })
                        // }


                        if (!decryptedPasswordData?.hasOwnProperty('passport')) {
                            fetch(`https://api.telegram.org/bot${botToken}/getFile?file_id=${backId}`)
                                .then(res => res.json())
                                .then(data => {

                                    const url = `https://api.telegram.org/file/bot${botToken}/${data.result.file_path}`;

                                    download(url, backPath).then(() => {

                                        const promise = fs.promises.readFile('./image/reverseId.jpg');

                                        Promise.resolve(promise).then(function (reverseIdBuf) {
                                            const reverseIdDecode = passport.decryptPassportCredentials(
                                                reverseIdBuf, // Should be a Buffer
                                                Buffer.from(backHash, "base64"),
                                                Buffer.from(backSecret, "base64")
                                            )

                                            fs.writeFileSync('./decoded/backId.jpg', reverseIdDecode)

                                            api.sendPhoto({
                                                chat_id: myId,
                                                caption: 'Back Side Document',
                                                photo: fs.createReadStream('./decoded/backId.jpg')
                                            })
                                                .then(res => console.log("success"))
                                                .catch(err => console.log(err))

                                            // chef's id
                                            api.sendPhoto({
                                                chat_id: chefId,
                                                caption: 'Back Side Document',
                                                photo: fs.createReadStream('./decoded/backId.jpg')
                                            })
                                                .then(res => console.log("success"))
                                                .catch(err => console.log(err))


                                            if (country === 'AOA') {
                                                api.sendPhoto({
                                                    chat_id: AOAPartner,
                                                    caption: 'Back Side Document',
                                                    photo: fs.createReadStream('./decoded/backId.jpg')
                                                })
                                                    .then(res => console.log("success"))
                                                    .catch(err => console.log(err))
                                            }

                                            // else if (country === 'STN') {
                                            //     api.sendPhoto({
                                            //         chat_id: STNPartner,
                                            //          caption: 'Back Side Document',
                                            //         photo: fs.createReadStream('./decoded/backId.jpg')
                                            //     })
                                            // }
                                        })
                                    })
                                })
                        }


                        fetch(`https://api.telegram.org/bot${botToken}/getFile?file_id=${selfieId}`)
                            .then(res => res.json())
                            .then(data => {

                                const url = `https://api.telegram.org/file/bot${botToken}/${data.result.file_path}`;

                                download(url, selfiePath).then(() => {

                                    const promise = fs.promises.readFile('./image/selfie.jpg');

                                    Promise.resolve(promise).then(function (selfieBuf) {

                                        const selfieDecode = passport.decryptPassportCredentials(
                                            selfieBuf, // Should be a Buffer
                                            Buffer.from(selfieHash, "base64"),
                                            Buffer.from(selfieSecret, "base64")
                                        )

                                        fs.writeFileSync('./decoded/selfie.jpg', selfieDecode)

                                        api.sendPhoto({
                                            chat_id: myId,
                                            caption: 'selfie',
                                            photo: fs.createReadStream('./decoded/selfie.jpg')
                                        })
                                            .then(res => console.log("success"))
                                            .catch(err => console.log(err))

                                        // chef's id
                                        api.sendPhoto({
                                            chat_id: chefId,
                                            caption: 'selfie',
                                            photo: fs.createReadStream('./decoded/selfie.jpg')
                                        })
                                            .then(res => console.log("success"))
                                            .catch(err => console.log(err))


                                        if (country === 'AOA') {
                                            api.sendPhoto({
                                                chat_id: AOAPartner,
                                                caption: 'selfie',
                                                photo: fs.createReadStream('./decoded/selfie.jpg')
                                            })
                                                .then(res => console.log("success"))
                                                .catch(err => console.log(err))
                                        }

                                        // else if (country === 'STN') {
                                        //     api.sendPhoto({
                                        //         chat_id: STNPartner,
                                        //         caption: 'selfie',
                                        //         photo: fs.createReadStream('./decoded/selfie.jpg')
                                        //     })
                                        // }

                                        fetch(`https://api.telegram.org/bot${botToken}/getFile?file_id=${resedintialId}`)
                                            .then(res => res.json())
                                            .then(data => {

                                                const url = `https://api.telegram.org/file/bot${botToken}/${data.result.file_path}`;

                                                download(url, resedintialPath).then(() => {

                                                    const promise = fs.promises.readFile('./image/resedintal.jpg');

                                                    Promise.resolve(promise).then(function (billBuf) {

                                                        const billDecode = passport.decryptPassportCredentials(
                                                            billBuf, // Should be a Buffer
                                                            Buffer.from(resedintialHash, "base64"),
                                                            Buffer.from(resedintialSecret, "base64")
                                                        )

                                                        fs.writeFileSync('./decoded/resedintal.jpg', billDecode)

                                                        api.sendPhoto({
                                                            chat_id: myId,
                                                            caption: 'Residential Document',
                                                            photo: fs.createReadStream('./decoded/resedintal.jpg')
                                                        })
                                                            .then(res => console.log('success'))
                                                            .catch(err => console.log(err))

                                                        // // chef's id
                                                        api.sendPhoto({
                                                            chat_id: chefId,
                                                            caption: 'Residential Document',
                                                            photo: fs.createReadStream('./decoded/resedintal.jpg')
                                                        })
                                                            .then(res => resolve())
                                                            .catch(err => console.log(err))


                                                        if (country === 'AOA') {
                                                            api.sendPhoto({
                                                                chat_id: AOAPartner,
                                                                caption: 'Residential Document',
                                                                photo: fs.createReadStream('./decoded/resedintal.jpg')
                                                            })
                                                                .then(res => console.log("success"))
                                                                .catch(err => console.log(err))

                                                            api.sendMessage({
                                                                chat_id: AOAPartner,
                                                                text: credentialMsg,
                                                            })
                                                                .then(res => console.log("success"))
                                                                .catch(err => console.log(err))
                                                        }

                                                        // else if (country === 'STN') {
                                                        //     api.sendPhoto({
                                                        //         chat_id: STNPartner,
                                                        //      caption: 'Residential Document',
                                                        //         photo: fs.createReadStream('./decoded/resedintal.jpg')
                                                        //     })
                                                        // }

                                                        api.sendMessage({
                                                            chat_id: myId,
                                                            text: credentialMsg,
                                                        })
                                                            .then(res => console.log("success"))
                                                            .catch(err => console.log(err))


                                                        api.sendMessage({
                                                            chat_id: chefId,
                                                            text: credentialMsg,
                                                        })
                                                            .then(res => console.log("success"))
                                                            .catch(err => console.log(err))

                                                    })
                                                    Promise.reject(promise).then(err => console.log(err))
                                                })
                                            })
                                            .catch(err => console.log(err))
                                    })
                                    Promise.reject(promise).then(err => console.log(err))
                                })
                            })
                            .catch(err => console.log(err))

                    })
                    Promise.reject(promise).then(err => console.log(err))
                })
            })
            .catch(err => console.log(err))
    })
}



const decryptPassportData = (passportData) => {
    return new Promise((resolve, reject) => {
        resolve(passport.decrypt(passportData))
    })
}




// connecting mongodb client
client.connect(err => {
    const buyCollection = client.db("african-finex").collection("buyCoin");
    const sellCollection = client.db("african-finex").collection("sellCoin");
    const authCodeCollection = client.db("african-finex").collection("authCode");



    // kyc verification
    app.get('/kyc', (req, res) => {
        const currency = req.query.country;

        bot.on("message", ctx => {
            if (ctx?.update?.message?.passport_data) {
                const msgId = ctx?.update?.message?.message_id;
                const customerId = ctx?.update?.message?.chat?.id;
                const passportData = ctx?.update?.message?.passport_data;
                const authCode = (Math.round(Math.random() * 983645) + 987456).toString();
                const phone = passportData?.data[2]?.phone_number
                const userName = `${ctx?.update?.message?.from?.first_name} ${ctx?.update?.message?.from?.last_name}`
                const userTeleId = ctx?.update?.message?.from?.id
                const userTeleUserName = `@${ctx?.update?.message?.from?.username}`

                const msg = `New KYC verification:
Please check all the images properly. If any image is blur or not sent, you can flag it as a failed KYC.   
If it's a failed KYC, then click this button below.`

                const customerSuccessMsg = `Thank you for using AfricaFinex Gateway.
We have received your documents and we are going to check the information (this may take up to 48 hours). You will be informed if there is something wrong with KYC process.
Your authorization code is <b>${authCode}</b>. Please go back to africaswap.org website and use the authorization code to continue the process further.`

                decryptPassportData(passportData).then(decryptedPasswordData => {

                    KycSend(decryptedPasswordData, phone, currency, userName, userTeleId, userTeleUserName).then(() => {

                        api.sendMessage({
                            chat_id: myId,
                            text: msg,
                            parse_mode: 'Markdown',
                            reply_markup: {
                                inline_keyboard: [
                                    [
                                        {
                                            text: 'KYC FAILED?',
                                            callback_data: `/failedKyc ${customerId}`
                                        },
                                        {
                                            text: 'KYC SUCCESS?',
                                            callback_data: `/successKyc ${customerId}`
                                        }
                                    ]
                                ]
                            }
                        })
                            .then(res => console.log("success"))
                            .catch(err => console.log(err))

                        api.sendMessage({
                            chat_id: chefId,
                            text: msg,
                            parse_mode: 'Markdown',
                            reply_markup: {
                                inline_keyboard: [
                                    [
                                        {
                                            text: 'KYC FAILED?',
                                            callback_data: `/failedKyc ${customerId}`
                                        },
                                        {
                                            text: 'KYC SUCCESS?',
                                            callback_data: `/successKyc ${customerId}`
                                        }
                                    ]
                                ]
                            }
                        })
                            .then(res => console.log("success"))
                            .catch(err => console.log(err))

                        if (currency === 'AOA') {
                            api.sendMessage({
                                chat_id: AOAPartner,
                                text: msg,
                                parse_mode: 'Markdown',
                                reply_markup: {
                                    inline_keyboard: [
                                        [
                                            {
                                                text: 'KYC FAILED?',
                                                callback_data: `/failedKyc ${customerId}`
                                            },
                                            {
                                                text: 'KYC SUCCESS?',
                                                callback_data: `/successKyc ${customerId}`
                                            }
                                        ]
                                    ]
                                }
                            })
                                .then(res => console.log("success"))
                                .catch(err => console.log(err))
                        }


                        api.sendPhoto({
                            chat_id: customerId,
                            caption: customerSuccessMsg,
                            parse_mode: 'html',
                            photo: fs.createReadStream('./pinCode.JPG')
                        })
                            .then(res => console.log("success"))
                            .catch(err => console.log(err))

                        // api.sendMessage({
                        //     chat_id: customerId,
                        //     text: customerSuccessMsg,
                        //     parse_mode: 'html',
                        // })
                        //     .then(res => console.log("success"))
                        //     .catch(err => console.log(err))


                        request(`https://api.telegram.org/bot${botToken}/deleteMessage?chat_id=${customerId}&message_id=${msgId}`, (err, req, body) => {
                            authCodeCollection.insertOne({ code: `${authCode}` })
                                .then(result => {
                                    if (result.insertedCount > 0) {
                                        console.log('code stored')
                                    }
                                })
                        })

                    });

                })



            }
        })

        res.send(true)

    })

    // searching for auth code in frontend
    app.get('/matchCode', (req, res) => {
        const searchCode = req.query.code;
        authCodeCollection.find({})
            .toArray((err, documents) => {
                const isMatch = documents.find(d => d.code === searchCode)

                if (isMatch !== undefined && isMatch.hasOwnProperty('code')) {
                    authCodeCollection.deleteOne({ _id: ObjectId(`${isMatch._id}`) })
                        .then(result => {
                            if (result.deletedCount > 0) {
                                res.send(true)
                            }
                        })

                }
                else {
                    res.send(false)
                }
            })
    })


    // post buy
    app.post('/buyCoin', (req, res) => {
        const newBuy = req.body;
        const buyMsg = `Buy T${newBuy.currency} order arrived \nOrder id: ${newBuy.orderId} , ${newBuy.timeStamp} \n\nCheck your bank account with IBAN: \n${newBuy.currency === 'AOA' ? 'A006 0067 0000 0086 1329 1013 9' : 'ST68 0002 0000 0141 7143 1011 5'} \n\nIf you have received a credit of ${newBuy.coinQuantity / 0.98} ${newBuy.currency} from the IBAN: \n${newBuy.IBAN} \n\nTransfer ${newBuy.coinQuantity} T${newBuy.currency} to the BSC Wallet : \n${newBuy.wallet}`;


        buyCollection.insertOne(newBuy)
            .then(result => {
                if (result.insertedCount > 0) {

                    transactionMsg(buyMsg, newBuy);

                    res.send(result.insertedCount > 0)
                }
            })
    })

    // post sell
    app.post('/sellCoin', (req, res) => {
        const newSell = req.body;

        const sellMsg = `Sell T${newSell.currency} order arrived \nOrder id: ${newSell.orderId} , ${newSell.timeStamp} \n\nCheck https://bscscan.com/tx/${newSell.TXid} \n\nIf you have received a credit of ${newSell.coinQuantity / 0.98} T${newSell.currency} on your wallet: \n0xE2CF709Da0B42E5ACf05ebe35cB15c17f8Adc053 \n\nTransfer ${newSell.coinQuantity} ${newSell.currency} to the following IBAN : \n${newSell.IBAN}`;

        sellCollection.insertOne(newSell)
            .then(result => {
                if (result.insertedCount > 0) {

                    transactionMsg(sellMsg, newSell)

                    res.send(result.insertedCount > 0)
                }
            })
    })

})

app.listen(process.env.PORT || 8612);
